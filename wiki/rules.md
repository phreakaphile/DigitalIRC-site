---
layout: wiki
title: Rules
---

1. No harassing the Ops,Admins or guests.
2. No flooding, this includes messaging an excessive amount of lines in channel or private message within a short interval of time.
3. No mass cloning. Mass Cloning is considered multiple connections from the same IP/host to the server. You are allowed to have 2 clones.
4. Good bots are allowed at the channel founders discression (Either botserv or your own eggdrop etc). If youre not the founder of a channel remember to ask the founder before you add the bot. DCC Bots are explicitly banned.
5. Don't ask for oper. We'll ask you.
6. No services abuse. Abuse is determined on a case-by-case basis.
7. Profanity must be kept to tolerable levels. Words deemed by the Admin to be inappropriate for channel depending on the context within which they are used, are forbidden.
8. Overall conduct should be ruled by common sense. If your behavior, playful or otherwise becomes too excessive, Ops/Opers at their discretion may ask that you tone it down, or may be forced to remove you.

Failing to follow any of the above rules can result in kicks, channel bans, kills, or network bans.

This server's staff reserve the right to deny access to anyone on this server without the benefit of prior warning or notification.

By connecting to and using this server you agree to abide and follow all rules, policies, and procedures.

All rules are subject to change without prior notice and are enforced at the staff's discression
