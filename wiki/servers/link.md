---
layout: wiki
title: Server link guide.
---

If you're interested in linking a server to DigitalIRC, one of the first things you should do is read our Guidelines. The Guidelines defines the administrative procedures and we expect prospective administrators to be familiar with it. It also provides a documented overview of the application process, voting process and server requirements.
This document can supplement to any requirements that are in the Guidelines. It can not remove requirements listed there, but it can extend them.

The next, but maybe even more important, step is to get to know your future colleagues. As you've read in the Guidelines, every administrator on this network has an equal vote in network decisions, so we're not very likely to accept a complete stranger. Take some time to hang around in #help & #TheZoo, speak with staff, help users and make yourself familiar with the way this network is run. You might also visit other official channels which you can find listed in various server MOTDs.

Of course, there's the technical part of the application. Your server should be available 24/7, with a stable connection and enough bandwidth available to IRCd. You should also make sure that our IRCd runs properly on the operating system of your choice. We run an IRCd called InspIRCd. You can get this source from the InspIRCd Project website at www.inspircd.org (Specifics will be given upon approval).

Once this is complete, double check the requirements listed below and fill in the form at the bottom of this page. Then send the application to our coordinator at linkage@digitalirc.org. After making sure all requirements are met the linkage team will start the linking process by sending a call for discussion (CFD) to the operator mailinglist. You will be contacted by our staff to testlink the server, and some tests will be performed to estimate its performance and stability during this period. The process will be concluded by a call for votes (CFV). During this period all current administrators will cast their vote, and the results will be announced on the mailinglist. When a majority votes in favour of your server, your testlink will be converted to a permanent one.

More detail regarding the entire process can be found in the Guidelines, section 5.1.

### Now, for a little more formal list of requirements:<br /><small>Special consideration may be given to links in Asia & Africa</small>

#### General Server Requirements:
* Your server must be available for the long term.
* Your nickname must have been registered for at least 2 months.
* You must have ports 6667, 6697 (ssl) open to the world for client connections and other ports available for server-to-server linkages
* You must be able to accept SSL connections for SSL encrypted traffic.
* Client servers must be capable of supporting at least 500 concurrent clients.
* You must be able to compile our ircd server software on your server. This requires POSIX compliance. Linux with a 2.6 kernel is strongly recommended.
* You must have a stable time base, either through ntpd or an other solution.
* You should not have other high bandwidth tasks running on the machine, such as an IRC daemon on a competing network, or a high traffic ftp or rsync mirror.

#### Technical Server Requirements:
* At least 256MB RAM for a dedicated machine. In the case that this is a virtual machine, the guest must have access to at least 256MB of ram. More will be required in the case that the machine handles other tasks.
* At least 3mbit of internet bandwidth, preferably with multiple uplinks or a Tier 1 backbone uplink.
* If you are using virtualization software, we require proof of a stable timebase. Xen with a stable time config is recommended.
* You should be able to run at least 50 background processes.

#### Overall Recommendations:
(While not strictly requirements, these are strongly recommended to make things easier for both you and us).
* A modern processor is strongly recommended for ease of replacing parts in case of failure.
* You should be comfortable compiling and configuring software on your machine with minimal assistance. In most cases, other admins and most opers will be able to assist you, but the final responsibility to make things go is yours.
* You should be familiar with our policies and some of our staff, server admins are preferable.
* You should understand how to update your machine to patch exploits, or make sure the root user does. As an IRC server, your machine will be somewhat high-profile and a target for attempted exploits.
* If you do need administrative assistance with your machine, it is recommended that you run the newest release of either Debian, Ubuntu or Gentoo linux.
* Please do not proceed with this application until you have fulfilled these requirements. Failure to do so will lead to your application being rejected.

continue with the [application form...]({{site.baseurl}}/wiki/servers/application/)