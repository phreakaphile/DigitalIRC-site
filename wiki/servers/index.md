---
layout: wiki
title: Servers
---

### Disclaimer
You should always try connecting via [irc.digitalirc.org](irc://irc.digitalirc.org) before attempting to connect to any of the following servers. If the domain of the server listed cannot be resolved, don't ask for the IP, just move onto another one.

### Ports
Use **6667** for standard connections & **6697** for secure (SSL) connections. 

### IPv6
The IPv6 connection address is [**ipv6.digitalirc.org**](irc://ipv6.digitalirc.org). Again, always attempt to use this address before connecting to individual IPv6 servers.


<div class="panel panel-default">
    <div class="panel-heading">Servers</div>
    <div class="panel-body">
    <h4>Region: North America</h4>
    <h5>Round robbin: us.irc.digitalirc.org  <a href="irc://us.irc.digitalirc.org/help" data-toggle="tooltip" data-placement="top" title="Plain text IRC connection"><span class="glyphicon glyphicon-link" aria-hidden="true"></span></a> <a href="ircs://us.irc.digitalirc.org:6697/help" data-toggle="tooltip" data-placement="top" title="Encrypted Connection"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></a> <a href="https://webchat.digitalirc.org/#help" data-toggle="tooltip" data-placement="top" title="HTTPS Webchat"><span class="glyphicon glyphicon-comment" aria-hidden="true"></span></a></h5>

<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>Address</th>
            <th>Location</th>
            <th>IPv6</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th>binary.digitalirc.org  <a href="irc://binary.digitalirc.org/help" data-toggle="tooltip" data-placement="top" title="Plain text IRC connection"><span class="glyphicon glyphicon-link" aria-hidden="true"></span></a> <a href="ircs://binary.digitalirc.org:6697/help" data-toggle="tooltip" data-placement="top" title="Encrypted Connection"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></a></th>
            <td>Canada</td>
            <td>Yes</td>
        </tr>
        <th>terra.digitalirc.org  <a href="irc://terra.digitalirc.org/help" data-toggle="tooltip" data-placement="top" title="Plain text IRC connection"><span class="glyphicon glyphicon-link" aria-hidden="true"></span></a> <a href="ircs://terra.digitalirc.org:6697/help" data-toggle="tooltip" data-placement="top" title="Encrypted Connection"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></a></th>
            <td>Los Angeles, California</td>
            <td>Yes</td>
        </tr>
        <th>tubes.digitalirc.org  <a href="irc://tubes.digitalirc.org/help" data-toggle="tooltip" data-placement="top" title="Plain text IRC connection"><span class="glyphicon glyphicon-link" aria-hidden="true"></span></a> <a href="ircs://tubes.digitalirc.org:6697/help" data-toggle="tooltip" data-placement="top" title="Encrypted Connection"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></a></th>
            <td>Houston, Texas</td>
            <td>No</td>
        </tr>
    </tbody>
</table>

    <h4>Region: Europe</h4>
    <h5>Round robbin: eu.irc.digitalirc.org  <a href="irc://eu.irc.digitalirc.org/help" data-toggle="tooltip" data-placement="top" title="Plain text IRC connection"><span class="glyphicon glyphicon-link" aria-hidden="true"></span></a> <a href="ircs://eu.irc.digitalirc.org:6697/help" data-toggle="tooltip" data-placement="top" title="Encrypted Connection"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></a> <a href="https://webchat.digitalirc.org/#help" data-toggle="tooltip" data-placement="top" title="HTTPS Webchat"><span class="glyphicon glyphicon-comment" aria-hidden="true"></span></a></h5>

<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>Address</th>
            <th>Location</th>
            <th>IPv6</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th>shire.digitalirc.org  <a href="irc://shire.digitalirc.org/help" data-toggle="tooltip" data-placement="top" title="Plain text IRC connection"><span class="glyphicon glyphicon-link" aria-hidden="true"></span></a> <a href="ircs://shire.digitalirc.org:6697/help" data-toggle="tooltip" data-placement="top" title="Encrypted Connection"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></a></th>
            <td>London, United Kingdom</td>
            <td>Yes</td>
        </tr>
        <tr>
            <th>mooo.digitalirc.org  <a href="irc://mooo.digitalirc.org/help" data-toggle="tooltip" data-placement="top" title="Plain text IRC connection"><span class="glyphicon glyphicon-link" aria-hidden="true"></span></a> <a href="ircs://mooo.digitalirc.org:6697/help" data-toggle="tooltip" data-placement="top" title="Encrypted Connection"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></a></th>
            <td>Paris, France</td>
            <td>Yes</td>
        </tr>
        <tr>
            <th>dynamo.digitalirc.org  <a href="irc://dynamo.digitalirc.org/help" data-toggle="tooltip" data-placement="top" title="Plain text IRC connection"><span class="glyphicon glyphicon-link" aria-hidden="true"></span></a> <a href="ircs://dynamo.digitalirc.org:6697/help" data-toggle="tooltip" data-placement="top" title="Encrypted Connection"><span class="glyphicon glyphicon-lock" aria-hidden="true"></span></a></th>
            <td>Strasbourg, France</td>
            <td>No</td>
        </tr>
    </tbody>
</table>
  </div>
  <!--<div class="panel-footer"></div>-->
</div>
<script>
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
})
</script>
