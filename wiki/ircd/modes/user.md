---
layout: wiki
title: User Modes
---
Mode | Description
---- | ----------:
c | Blocks private messages and notices from users who do not share a common channel with you.
g | In combination with /allow, provides for server side ignore.
h | Marks as 'available for help' in WHOIS (IRCop only, requires helpop module).
i | Makes invisible to /WHO if the user using /WHO is not in a common channel.
k | Prevents the user from being kicked from channels, or having op modes removed from them (services only).
o | Marks as a IRC operator.
s [mask] | Receives server notices specified by [mask] (IRCop only).
r | Marks as a having a registered nickname.
w | Receives wallops messages.
x | Gives a cloaked hostname.
B | Marks as a bot.
G | Censors messages sent to the user based on filters configured for the network.
H | Hides an oper's oper status from WHOIS.
I | Hides a user's entire channel list in WHOIS from non-IRCops.
R | Blocks private messages from unregistered users.
S | Strips mIRC color/bold/underline codes out of private messages to the user.
W | Receives notification when a user uses WHOIS on them (IRCop only).
