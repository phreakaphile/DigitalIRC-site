---
layout: wiki
title: Modes
---

<table class="table">
    <thead>
        <tr>
            <th>Mode Type</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <th><a href="{{site.baseurl}}/wiki/ircd/modes/channel/">Channel Modes</a></th>
            <td>Modes which control how the channel behaves. e.g. Invite only</td>
        </tr>
        <tr>
            <th><a href="{{site.baseurl}}/wiki/ircd/modes/user/">User Modes</a></th>
            <td>Modes which control how a user behaves. e.g. Has hidden host</td>
        </tr>
        <tr>
            <th><a href="{{site.baseurl}}/wiki/ircd/modes/snomasks/">Server Masks</a></th>
            <td>Modes which control how operators get additional information. e.g. Oper up's</td>
        </tr>
    </tbody>
</table>
