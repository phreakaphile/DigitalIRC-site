---
layout: wiki
title: Network Bans
---

So you cannot connect to the network and your getting an error saying you were G/K/Z-Lined.

Emails from the ticket system will be sent from *noreply@digitalirc.org* make sure that this isn't in your spam folder before opening another ticket, you may also be asked to provide the IP/Hostname you were using when you were banned.

### Appealing a bot ban

While we do allow bots on DigitalIRC, bots that are abusive are banned. These include:
* Flood bots
* Take-over bots

Additionally if an unknown bot joins any official channels and its owner cannot be found it will be removed from the network.

To appeal the ban contact an operator in [#help]({{site.webchat}}/help) or use the [ticket system]({{site.baseurl}}/tickets/) *Bot-Unban* in the subject line.

### Appealing a K-line

Unlike other bans used on DigitalIRC k-lines only effect a single server, to appeal this you will need to contact the server owner. This can be found by connecting to a different server and doing */admin SERVER*. Replace SERVER with the server you were banned on.

To appeal the ban contact an operator in [#help]({{site.webchat}}/help) or use the [ticket system]({{site.baseurl}}/tickets/) with the subject *kline-ban SERVER* please replace SERVER with the server you were banned on.

### Appealing a G-line

This is a global, network wide ban on a host. This is only to remove users temporarily from the network. If you join IRC via a proxy or bouncer, and ask for a G-line to be removed the original g-line will be extended and the IP you connected to the network with will be banned as well.

To appeal use the [ticket system]({{site.baseurl}}/tickets/) *gline-ban* in the subject line.

### Appealing a Z-line

This is a global, network wide ban on an IP or IP Range. This is only used to permanently remove user(s) from the network after repeated warnings or ban evasion or it is also used by our network proxy monitor to remove users connecting via open proxies or tor.
If you join IRC via a proxy or bouncer, and ask for a Z-line to be removed the original g-line will remain and and the IP you connected to the network with will be banned as well.

To appeal use the [ticket system]({{site.baseurl}}/tickets/) with *Zline-ban* in the subject line.
