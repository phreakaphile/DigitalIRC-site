---
layout: wiki
title: Services
---

DigitalIRC currently uses atheme for it's services package with a [web frontend available]({{site.baseurl}}/services/). Additionally we run denora statserv for providing channel statistics (web frontend [available here]({{site.baseurl}}/stats/))

### [NickServ]({{site.baseurl}}/wiki/services/nickserv/)

Nickname management service that provides [nickname registration]({{site.baseurl}}/wiki/services/nickserv/register/), and is required to access most features in the rest of service's that atheme provides.

### [Chanserv]({{site.baseurl}}/wiki/services/chanserv/)

Chanserv provides channel management, including but not limited to, access control, entry message, channel topic control and user banning. All channels that wish to use chanserv's features must [register]({{site.baseurl}}/wiki/services/chanserv/register/) with chanserv

### [HostServ]({{site.baseurl}}/wiki/services/hostserv/)

Allows users to show custom [vHosts]({{site.baseurl}}/wiki/services/hostserv/request/) (virtual hosts) instead of their real IP address or alternative cloaked host (e.g. DigIRC-64602D23.someISP.com can become Someuser.user.digitalirc.org). On DigitalIRC when ever an account is registered with NickServ a vHost is set on the account of UserName.user.digitalirc.org, where UserName is the nickname you were using when you registered the account.

### [MemoServ]({{site.baseurl}}/wiki/services/memoserv/)

MemoServ implements the sending of short messages (memos) to offline users. The service will keep the memos while the user is offline and allow them to be requested and read by the user at any later time.

### [BotServ]({{site.baseurl}}/wiki/services/botserv/)

Allows users to get a personal [Request a custom bot]({{site.baseurl}}/wiki/services/botserv/request/) into their channels with additional channel management capabilities, e.g. additional monitoring of the channels against floods, repetitions, caps writing, and swearing, and taking appropriate actions. It might also implement additional commands, e.g. also some that are said aloud in the channel (like !op, !deop, !voice, !devoice, !kick, and many others) or send a short greet message when an user joins a channel, or even "take over" typical ChanServ actions such as auto-opping users, saying the entry notice to be set by the channel owner and so on.

### [OperServ]({{site.baseurl}}/wiki/services/operserv/)

The IRCops' and IRC admins' black box, that e.g. allows them to manage the list of network bans, send global messages to all users or to specific users on login, to set modes or kick users from any channel and other things.

### [StatServ]({{site.baseurl}}/wiki/services/statserv/)

Statserv allows you to monitor your channel with the goal to create a statistical overview of the activity. Most of the information is used to create the SolidIRC Network Statistics Website, which is free to use for everyone, but there are some IRC Commands too.

**Note: Statserv only counts events like words or lines written; it will not log the conversations of a channel.**
