---
layout: wiki
title: Hostserv request
---

<h3>What is a vhost</h3>
A vhost (a.k.a. vanity host or virtual hosts) is a custom part after the @ in your user mask on IRC. For example my hostmask is:
> MrRandom!me@MrRandom.may.explode.on.impact
My vhost when I use the nickname MrRandom is "MrRandom.my.explode.on.impact.

<h3>What vhosts can I get</h3>

With our recent move to atheme as our services package, all registered accounts have the default vhost assigned to them when they register "yournickname.users.digitalirc.org" but with atheme's hostserv (the service we use to manage vhosts) it allows use to now have an offer list, this list contains vhosts which any user can assign to them selves without any operator intervention.

The list of vhosts on the offer list is:
* $account.may.explode.on.impact
* $account.users.digitalirc.org (set by default on new accounts)
* $account.loves.porn

Some offered vhosts are hidden unless you are part of the group it was for. To see the full list of vhosts you can get do:
> /hs offerlist

To get a vhost off the offers list (when identified to services) do:
> /msg hostserv take [offer vhost you want]
**remember to include the $account part since that is important**

<h3>What if I want a custom vhost/a vhost that contains a domain name.</h3>
For all domain names that aren't on the offers list submit a hostserv request which will be reviewed by the Server Operators. The decisions of the operators are final and absolute.

All vhosts applied for are subject to the network rules on profanity and common sense guidelines

If you think your vhost meets these simple requirements simply do:
> /msg hostserv request [vhost you want]

If you want a vhost that ends in a valid TLD it will be auto-rejected by the system. If you do want a vhost for a valid domain please ask in #help for an oper to assign it.

For a valid domain you must be able to prove you have access/own the relevent domain name.
