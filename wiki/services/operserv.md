---
layout: wiki
title: OperServ
---

<a name="AKILL"><h4>AKILL</h4></a>
<p>
AKILL allows you to maintain network-wide bans.
Services will keep your AKILLs stored and allow for easy management.
<p>
<strong>Syntax:</strong> <tt>AKILL ADD &lt;nick|hostmask&gt; [!P|!T &lt;minutes&gt;] &lt;reason&gt;</tt><br>
<p>
If the !P token is specified the AKILL will never expire (permanent).
If the !T token is specified expire time must follow, in minutes,
hours ("h"), days ("d") or weeks ("w").
<p>
<strong>Examples:</strong>
<br><tt>/msg OperServ AKILL ADD foo !T 5 bar reason</tt>
<br><tt>/msg OperServ AKILL ADD foo !T 3d bar reason</tt>
<br><tt>/msg OperServ AKILL ADD foo@bar.com !P foo reason</tt>
<br><tt>/msg OperServ AKILL ADD foo@bar.com foo reason</tt>
<p>
The first example looks for the user with a nickname of "foo" and adds
a 5 minute AKILL for "bar reason."
<p>
The second example is similar but adds the AKILL for 3 days instead of
5 minutes.
<p>
The third example adds a permanent AKILL on foo@bar.com for "foo reason."
<p>
The fourth example adds a AKILL on foo@bar.com for the duration specified
in the configuration file for "foo reason."
<p>
<strong>Syntax:</strong> <tt>AKILL DEL &lt;hostmask|number&gt;</tt><br>
<p>
If number is specified it correlates with the number on AKILL LIST.
You may specify multiple numbers by separating with commas.
You may specify a range by using a colon.
<p>
<strong>Examples:</strong>
<br><tt>/msg OperServ AKILL DEL foo@bar.com</tt>
<br><tt>/msg OperServ AKILL DEL 5</tt>
<br><tt>/msg OperServ AKILL DEL 1,2,5,10</tt>
<br><tt>/msg OperServ AKILL DEL 1:5,7,9:11</tt>
<p>
<strong>Syntax:</strong> <tt>AKILL LIST [FULL]</tt><br>
<p>
If FULL is specified the AKILL reasons will be shown.
<p>
<strong>Examples:</strong>
<br><tt>/msg OperServ AKILL LIST</tt>
<br><tt>/msg OperServ AKILL LIST FULL</tt>
<p>
<strong>Syntax:</strong> <tt>AKILL LIST &lt;hostmask&gt;</tt><br>
<p>
Shows any AKILLs matching the given hostmask, with reasons.
This command will not perform DNS lookups on a host,
for best results repeat it with host and IP address.
<p>
<strong>Examples:</strong>
<br><tt>/msg OperServ AKILL LIST test@192.168.1.1</tt>
<p>
<strong>Syntax:</strong> <tt>AKILL LIST &lt;number&gt;</tt><br>
<p>
Shows the given AKILL, with reason.
<p>
<strong>Syntax:</strong> <tt>AKILL SYNC</tt><br>
<p>
Sends all akills to all servers. This can be useful in case
services will be down or do not see a user as matching a
certain akill.
<a name="CLEARCHAN"><h4>CLEARCHAN</h4></a>
<p>
CLEARCHAN allows operators to clear
a channel in one of three ways: KICK,
which kicks all users from the channel, KILL,
which kills all users in the channel off the
network, or AKILL, which sets a one week
network ban against the hosts of all users in
the channel.
<p>
This command should not be used lightly.
<p>
<strong>Syntax:</strong> <tt>CLEARCHAN KICK|KILL|AKILL &lt;#channel&gt; &lt;reason&gt;</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ CLEARCHAN KICK #warez warez is bad, mk?</tt>
<br><tt>/msg OperServ CLEARCHAN AKILL #warez you were warned!</tt>
<a name="CLONES"><h4>CLONES</h4></a>
<p>
CLONES keeps track of the number of clients
per IP address. Warnings are displayed in
the snoop channel about IP addresses with
multiple clients.
<p>
CLONES only works on clients whose IP address
Atheme knows. If the ircd does not support
propagating IP addresses at all, CLONES is
not useful; if IP addresses are not sent for
spoofed clients, those clients are exempt from
CLONES checking.
<p>
<strong>Syntax:</strong> <tt>CLONES KLINE ON|OFF</tt><br>
<p>
Enables/disables banning IP addresses with more
than the allowed number clients from the network
for one hour (these bans are not added to the
AKILL list). This setting is saved in etc/services.db
and defaults to off.
<p>
<strong>Syntax:</strong> <tt>CLONES LIST</tt><br>
<p>
Shows all IP addresses with more than 3 clients
with the number of clients and whether the IP
address is exempt.
<p>
<strong>Syntax:</strong> <tt>CLONES ADDEXEMPT &lt;ip&gt; &lt;clones&gt; [!P|!T &lt;minutes&gt;] &lt;reason&gt;</tt><br>
<p>
Adds an IP address to the clone exemption list.
The IP address must match exactly with the form
used by the ircd (mind '::' shortening with IPv6).
The IP address can also be a CIDR mask, for example
192.168.1.0/24. Single IPs take priority above CIDR.
&lt;clones&gt; is the number of clones allowed; it must be
at least 4. Warnings are sent if this number is
met, and a network ban may be set if the number
is exceeded.
The reason is shown in LISTEXEMPT.
The clone exemption list is stored in etc/services.db.
<p>
<strong>Syntax:</strong> <tt>CLONES DELEXEMPT &lt;ip&gt;</tt><br>
<p>
Removes an IP address from the clone exemption list.
<p>
<strong>Syntax:</strong> <tt>CLONES SETEXEMPT [DEFAULT | &lt;ip&gt;] &lt;ALLOWED | WARN&gt; &lt;limit&gt;</tt><br>
<p>
Sets either the default or a given exemption's ALLOWED or
WARN limit to the specified number of clones. WARN or ALLOWED
can be 0, disabling any warning messages or kills.
<p>
<strong>Syntax:</strong> <tt>CLONES SETEXEMPT &lt;ip&gt; &lt;REASON | DURATION&gt; &lt;value&gt;</tt><br>
<p>
Sets the reason or duration of a given exemption to the
specified value. The DURATION value can be 0, making the
exemption permanent.
<p>
<strong>Syntax:</strong> <tt>CLONES LISTEXEMPT</tt><br>
<p>
Shows the clone exemption list with reasons.
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ CLONES ADDEXEMPT 127.0.0.1 100 local</tt>
<br><tt>/msg OperServ CLONES DELEXEMPT 192.168.1.2</tt>
<p>
<strong>Syntax:</strong> <tt>CLONES DURATION</tt><br>
<p>
Allows modifying the duration that hosts who clone
are banned for. Defaults to one hour. Is saved between
restarts.
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ CLONES DURATION 30m</tt>
<p>
<a name="COMPARE"><h4>COMPARE</h4></a>
<p>
COMPARE allows operators with chan:auspex
privilege to view matching information
on two users, or two channels.
<p>
It is useful in clone detection,
amongst other situations.
<p>
<strong>Syntax:</strong> <tt>COMPARE &lt;#channel|user&gt; &lt;#channel|user&gt;</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ COMPARE #warez #dcc</tt>
<br><tt>/msg OperServ COMPARE w00t Brik</tt>
<a name="GREPLOG"><h4>GREPLOG</h4></a>
<p>
GREPLOG searches through services logs and
displays matching lines.
<p>
The first parameter is either a service name
(to search all commands given to that service)
or an asterisk (to search all changes to services
data).
<p>
The second parameter is the pattern to search for.
It may contain * and ? wildcards and should usually
start and end in *.
<p>
The optional third parameter is the number of
previous days to search in addition to today.
<p>
Note that this command will only work if sufficient
information is written to log files.
<p>
<strong>Syntax:</strong> <tt>GREPLOG &lt;service&gt; &lt;pattern&gt; [days]</tt><br>
<strong>Syntax:</strong> <tt>GREPLOG * &lt;pattern&gt; [days]</tt><br>
<p>
<strong>Examples:</strong>
<br><tt>/msg OperServ GREPLOG ChanServ *#somechan* 7</tt>
<br><tt>/msg OperServ GREPLOG * *#somechan* 60</tt>
<a name="IDENTIFY"><h4>IDENTIFY</h4></a>
<p>
IDENTIFY authenticates for services operator
privileges, if the operator or operator class
has been defined as needing an additional
password.
<p>
You need to log in to your services account first.
<p>
<strong>Syntax:</strong> <tt>IDENTIFY &lt;password&gt;</tt><br>
<a name="IGNORE"><h4>IGNORE</h4></a>
<p>
Services has an ignore list which functions similarly to
the way a user can ignore another user. If a user matches
a mask in the ignore list and attempts to use services,
they will not get a reply.
<p>
<br><tt>ADD   - Add a mask to the ignore list.</tt>
<br><tt>DEL   - Delete a mask from the ignore list.</tt>
<br><tt>LIST  - List all the entries in the ignore list.</tt>
<br><tt>CLEAR - Clear all the entries in the ignore list.</tt>
<p>
<strong>Examples:</strong>
<br><tt>/msg OperServ IGNORE ADD pfish!*@* flooding services</tt>
<br><tt>/msg OperServ IGNORE DEL pfish!*@*</tt>
<br><tt>/msg OperServ IGNORE LIST</tt>
<br><tt>/msg OperServ IGNORE CLEAR</tt>
<a name="INFO"><h4>INFO</h4></a>
<p>
INFO shows some services configuration information
that is not available to see elsewhere.
<p>
<strong>Syntax:</strong> <tt>INFO</tt><br>
<a name="INJECT"><h4>INJECT</h4></a>
<p>
INJECT fakes data from the uplink.  This
command is for debugging only and should
not be used unless you know what you're doing.
<p>
<strong>Syntax:</strong> <tt>INJECT &lt;parameters&gt;</tt><br>
<p>
<strong>Examples:</strong>
<br><tt>/msg OperServ INJECT :uplink.network.com PING :shrike.network.com</tt>
<br><tt>/msg OperServ INJECT :foo PRIVMSG OperServ :HELP INJECT</tt>
<a name="JUPE"><h4>JUPE</h4></a>
<p>
JUPE introduces a fake server with the given name, so
that the real server cannot connect. Jupes only last
as long as services is connected to the uplink and
can also (on most ircds) be removed with a simple
/squit command.
<p>
<strong>Syntax:</strong> <tt>JUPE &lt;server&gt; &lt;reason&gt;</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ JUPE irc.blah.net very unstable server</tt>
<a name="MODE"><h4>MODE</h4></a>
<p>
MODE allows for the editing of modes on a channel. Some networks
will most likely find this command to be unethical.
<p>
<strong>Syntax:</strong> <tt>MODE &lt;#channel&gt; &lt;mode&gt; [parameters]</tt><br>
<p>
<strong>Examples:</strong>
<br><tt>/msg OperServ MODE #heh -m</tt>
<br><tt>/msg OperServ MODE #heh +o foo</tt>
<a name="MODINSPECT"><h4>MODINSPECT</h4></a>
<p>
MODINSPECT displays detailed information about a module.
<p>
The names can be gathered from the MODLIST command. They
are not necessarily equal to the pathnames to load them
with MODLOAD.
<p>
<strong>Syntax:</strong> <tt>MODINSPECT &lt;name&gt;</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ MODINSPECT protocol/charybdis</tt>
<a name="MODLIST"><h4>MODLIST</h4></a>
<p>
MODLIST displays a listing of all loaded
modules and their addresses.
<p>
<strong>Syntax:</strong> <tt>MODLIST</tt><br>
<a name="MODLOAD"><h4>MODLOAD</h4></a>
<p>
MODLOAD loads one or more modules.
<p>
If the path does not start with a slash, it is taken
relative to PREFIX/modules or PREFIX/lib/atheme/modules
(depending on how Atheme was compiled). Specifying a
suffix like .so is optional.
<p>
If any of the modules need a rehash after being
loaded, this is done automatically.
<p>
<strong>Syntax:</strong> <tt>MODLOAD &lt;path...&gt;</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ MODLOAD ../contrib/fc_dice</tt>
Help for ^BMODRELOAD^B:
<p>
MODRELOAD reloads a currently loaded module. If the command fails,
the module in question will be unloaded until errors are corrected.
<p>
<strong>Syntax:</strong> <tt>MODRELOAD &lt;name...&gt;</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ MODRELOAD chanserv/register</tt>
<a name="MODUNLOAD"><h4>MODUNLOAD</h4></a>
<p>
MODUNLOAD unloads one or more modules. Not all modules
can be unloaded.
<p>
The names can be gathered from the MODLIST command. They
are not necessarily equal to the pathnames to load them
with MODLOAD.
<p>
<strong>Syntax:</strong> <tt>MODUNLOAD &lt;name...&gt;</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ MODUNLOAD chanserv/register</tt>
<a name="NOOP"><h4>NOOP</h4></a>
<p>
NOOP allows you to deny IRCop access on a per-hostmask
or per-server basis. If a matching user opers up, they
will be killed.
<p>
<strong>Syntax:</strong> <tt>NOOP ADD HOSTMASK &lt;nick!user@host&gt; [reason]</tt><br>
<strong>Syntax:</strong> <tt>NOOP ADD SERVER &lt;mask&gt; [reason]</tt><br>
<p>
<strong>Examples:</strong>
<br><tt>/msg OperServ NOOP ADD HOSTMASK *!*@some.spoof Abusive operator</tt>
<br><tt>/msg OperServ NOOP ADD SERVER bad.server Abusive admin</tt>
<p>
<strong>Syntax:</strong> <tt>NOOP DEL HOSTMASK &lt;nick!user@host&gt;</tt><br>
<strong>Syntax:</strong> <tt>NOOP DEL SERVER &lt;mask&gt;</tt><br>
<p>
<strong>Examples:</strong>
<br><tt>/msg OperServ NOOP DEL HOSTMASK *!some@operator.host</tt>
<br><tt>/msg OperServ NOOP DEL SERVER bad.server</tt>
<p>
<strong>Syntax:</strong> <tt>NOOP LIST HOSTMASK</tt><br>
<strong>Syntax:</strong> <tt>NOOP LIST SERVER</tt><br>
<a name="OVERRIDE"><h4>OVERRIDE</h4></a>
<p>
OVERRIDE is used for running a command as another user.
<p>
<strong>Syntax:</strong> <tt>OVERRIDE &lt;target&gt; &lt;service&gt; &lt;command&gt; [params]</tt><br>
<p>
<strong>Examples:</strong>
<br><tt>/msg OperServ OVERRIDE dotslasher ChanServ FLAGS #cows nenolod +*</tt>
<a name="RAKILL"><h4>RAKILL</h4></a>
<p>
RAKILL allows for regex-based akills,
which are useful for removing clones
or botnets. The akills are not added
to OperServ's list and last a week.
<p>
Be careful, as regex is very easy to
make mistakes with. Use RMATCH first.
The regex syntax is exactly the same.
<p>
<strong>Syntax:</strong> <tt>RAKILL /&lt;pattern&gt;/[i][p] &lt;reason&gt;</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ RAKILL /^m[oo|00]cow/i No moocows allowed.</tt>
<a name="RAW"><h4>RAW</h4></a>
<p>
RAW injects data into the uplink.  This
command is for debugging only and should
not be used unless you know what you're
doing.
<p>
<strong>Syntax:</strong> <tt>RAW &lt;parameters&gt;</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ RAW :OperServ OPERWALL :My admin is a loser</tt>
<a name="READONLY"><h4>READONLY</h4></a>
<p>
READONLY allows services operators to enable
or disable readonly mode while services is
running.
<p>
<strong>Syntax:</strong> <tt>READONLY ON|OFF</tt><br>
<p>
<strong>Examples:</strong>
<br><tt>/msg OperServ READONLY ON</tt>
<a name="REHASH"><h4>REHASH</h4></a>
<p>
REHASH updates the database and reloads
the configuration file.  You can perform
a rehash from system console with a kill -HUP
command.
<p>
<strong>Syntax:</strong> <tt>REHASH</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ REHASH</tt>
<a name="RESTART"><h4>RESTART</h4></a>
<p>
RESTART shuts down services and restarts them.
<p>
<strong>Syntax:</strong> <tt>RESTART</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ RESTART</tt>
<a name="RMATCH"><h4>RMATCH</h4></a>
<p>
RMATCH shows all users whose
<u>nick</u>!<u>user</u>@<u>host</u> <u>gecos</u>
matches the given regular expression.
<p>
Instead of a slash, any character that
is not a letter, digit, whitespace or
backslash and does not occur in the pattern
can be used. An i after the pattern means
case insensitive matching.
<p>
By default, the pattern is a POSIX extended
regular expression. If PCRE support has been
compiled in, you can put a p after the pattern
to use it.
<p>
By default, there is a limit on the number
of matches. To override this limit, add
the FORCE keyword. In any case the actual
number of matches will be shown.
<p>
<strong>Syntax:</strong> <tt>RMATCH /&lt;pattern&gt;/[i][p] [FORCE]</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ RMATCH /^m(oo|00)cow/i FORCE</tt>
<br><tt>/msg OperServ RMATCH #^[a-z]+!~?[a-z]+@#</tt>
<br><tt>/msg OperServ RMATCH /^[^ ]* [^ ]*$/</tt>
<br><tt>/msg OperServ RMATCH /\d\d\d/p</tt>
<a name="RNC"><h4>RNC</h4></a>
<p>
RNC shows the most common realnames on the network.
<p>
<strong>Syntax:</strong> <tt>RNC [number]</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ RNC 10</tt>
<a name="RWATCH"><h4>RWATCH</h4></a>
<p>
RWATCH maintains a list of regular expressions,
which the <u>nick</u>!<u>user</u>@<u>host</u> <u>gecos</u>
of all connecting clients are matched against.
Matching clients can be displayed in the snoop
channel and/or banned from the network. These
network bans are set on *@host, last 24 hours
and are not added to the AKILL list.
The RWATCH list is stored in etc/rwatch.db and
saved whenever it is modified.
<p>
See RMATCH for more information about regular
expression syntax.
<p>
<strong>Syntax:</strong> <tt>RWATCH ADD /&lt;pattern&gt;/[i][p] &lt;reason&gt;</tt><br>
<p>
Adds a regular expression to the RWATCH list.
The reason is shown in snoop notices and kline reasons.
<p>
<strong>Syntax:</strong> <tt>RWATCH DEL /&lt;pattern&gt;/[i][p]</tt><br>
<p>
Removes a regular expression from the RWATCH list.
<p>
<strong>Syntax:</strong> <tt>RWATCH LIST</tt><br>
<p>
Shows the RWATCH list. The meaning of the letters is:
<br><tt>i - case insensitive match</tt>
<br><tt>p - PCRE pattern</tt>
<br><tt>S - matching clients are shown in the snoop channel</tt>
<br><tt>K - matching clients are banned from the network</tt>
<p>
<strong>Syntax:</strong> <tt>RWATCH SET /&lt;pattern&gt;/[i][p] &lt;options&gt;</tt><br>
<p>
Changes the action for a regular expression. Possible
values for &lt;options&gt; are:
<br><tt>SNOOP   - enables display in the snoop channel</tt>
<br><tt>NOSNOOP - disables display in the snoop channel</tt>
<br><tt>KLINE   - enables network bans</tt>
<br><tt>NOKLINE - disables network bans</tt>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ RWATCH ADD /^m(oo|00)cow/i moocow figure</tt>
<br><tt>/msg OperServ RWATCH DEL /^m(oo|00)cow/i</tt>
<a name="SCRIPT"><h4>SCRIPT</h4></a>
<p>
SCRIPT allows you to manage Atheme scripts. Currently,
Atheme is only scriptable in Perl.
<p>
Use MODLOAD/MODUNLOAD/MODRELOAD for Atheme C modules.
<p>
<strong>Syntax:</strong> <tt>SCRIPT LOAD &lt;filename&gt;</tt><br>
<p>
The filename must be a full (absolute) path to the Perl
script you would like to load.
<p>
<strong>Examples:</strong>
<br><tt>/msg OperServ SCRIPT LOAD /home/services/scripts/cs_hello.pl</tt>
<p>
<strong>Syntax:</strong> <tt>SCRIPT UNLOAD &lt;filename&gt;</tt><br>
<p>
The filename must be a full (absolute) path to the Perl
script you would like to unload
<p>
<strong>Examples:</strong>
<br><tt>/msg OperServ SCRIPT UNLOAD /home/services/scripts/cs_hello.pl</tt>
<p>
<strong>Syntax:</strong> <tt>SCRIPT LIST</tt><br>
<p>
LIST simply lists all currently loaded Perl scripts.
<p>
<strong>Examples:</strong>
<br><tt>/msg OperServ SCRIPT LIST</tt>
<a name="SET AKICKTIME"><h4>SET AKICKTIME</h4></a>
<p>
SET AKICKTIME allows network staff to the default duration
of akicks. Setting this to 0 makes all akicks permanent unless
a duration is specified in the AKICK command.
<p>
<strong>Syntax:</strong> <tt>SET AKICKTIME &lt;time in minutes&gt;</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ SET AKICKTIME 20</tt>
<a name="SET CHANEXPIRE"><h4>SET CHANEXPIRE</h4></a>
<p>
SET CHANEXPIRE allows network staff to modify how often channel
expirations will be checked.
<p>
<strong>Syntax:</strong> <tt>SET CHANEXPIRE &lt;time in days&gt;</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ SET CHANEXPIRE 30</tt>
<a name="SET COMMITINTERVAL"><h4>SET COMMITINTERVAL</h4></a>
<p>
SET COMMITINTERVAL allows network staff to set how often (in minutes)
the services database will be written to disk.
<p>
<strong>Syntax:</strong> <tt>SET COMMITINTERVAL &lt;time in minutes&gt;</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ SET COMMITINTERVAL 5</tt>
<a name="SET ENFORCEPREFIX"><h4>SET ENFORCEPREFIX</h4></a>
<p>
SET ENFORCEPREFIX changes the prefix of the nick
that a user will be changed to when they use an
enforced nickname and fail to authenticate to it.
<p>
<strong>Syntax:</strong> <tt>SET ENFORCEPREFIX &lt;prefix&gt;</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ SET ENFORCEPREFIX Guest</tt>
<a name="SET KLINETIME"><h4>SET KLINETIME</h4></a>
<p>
SET KLINETIME allows network staff to set a default time before
AKILLs with no provided duration will expire.
<p>
<strong>Syntax:</strong> <tt>SET KLINETIME &lt;time in days&gt;</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ SET KLINETIME 5</tt>
<a name="SET MAXCHANACS"><h4>SET MAXCHANACS</h4></a>
<p>
SET MAXCHANACS allows setting the maximum number of entries
allowed in a channel's access list.
<p>
<strong>Syntax:</strong> <tt>SET MAXCHANACS &lt;value&gt;</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ SET MAXCHANACS 30</tt>
<a name="SET MAXCHANS"><h4>SET MAXCHANS</h4></a>
<p>
SET MAXCHANS allows setting how many channels one account may
be founder of.
<p>
<strong>Syntax:</strong> <tt>SET MAXCHANS &lt;value&gt;</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ SET MAXCHANS 7</tt>
<a name="SET MAXFOUNDERS"><h4>SET MAXFOUNDERS</h4></a>
<p>
SET MAXFOUNDERS allows setting the maximum number of founders
that one channel may have.
<p>
<strong>Syntax:</strong> <tt>SET MAXFOUNDERS &lt;value&gt;</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ SET MAXFOUNDERS 4</tt>
<a name="SET MAXLOGINS"><h4>SET MAXLOGINS</h4></a>
<p>
SET MAXLOGINS allows setting how many users may be logged into
one account at the same time.
<p>
<strong>Syntax:</strong> <tt>SET MAXLOGINS &lt;value&gt;</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ SET MAXLOGINS 7</tt>
<a name="SET MAXNICKS"><h4>SET MAXNICKS</h4></a>
<p>
SET MAXNICKS allows setting how many nicknames one account
is allowed to own.
<p>
<strong>Syntax:</strong> <tt>SET MAXNICKS &lt;value&gt;</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ SET MAXNICKS 7</tt>
<a name="SET MAXUSERS"><h4>SET MAXUSERS</h4></a>
<p>
SET MAXUSERS allows setting how many accounts one email
address may have registered. This can be overridden on
a per-email basis with the emailexempts configuration
block.
<p>
<strong>Syntax:</strong> <tt>SET MAXUSERS &lt;value&gt;</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ SET MAXUSERS 7</tt>
<a name="SET MDLIMIT"><h4>SET MDLIMIT</h4></a>
<p>
SET MDLIMIT sets how many pieces of metadata can belong
to one object (an account, group or channel).
<p>
<strong>Syntax:</strong> <tt>SET MDLIMIT &lt;value&gt;</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ SET MDLIMIT 30</tt>
<a name="SET NICKEXPIRE"><h4>SET NICKEXPIRE</h4></a>
<p>
SET NICKEXPIRE allows network staff to modify how often nickname
and account expirations will be checked.
<p>
<strong>Syntax:</strong> <tt>SET NICKEXPIRE &lt;time in days&gt;</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ SET NICKEXPIRE 30</tt>
<a name="SET RECONTIME"><h4>SET RECONTIME</h4></a>
<p>
SET RECONTIME allows modifying how long before services will
try to reconnect to the uplink.
<p>
<strong>Syntax:</strong> <tt>SET RECONTIME &lt;time in seconds&gt;</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ SET RECONTIME 5</tt>
<a name="SET SPAM"><h4>SET SPAM</h4></a>
<p>
SET SPAM allows network staff to define whether or not new users
get messaged about services when they connect.
<p>
<strong>Syntax:</strong> <tt>SET SPAM TRUE|FALSE</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ SET SPAM TRUE</tt>
<a name="SGLINE"><h4>SGLINE</h4></a>
<p>
SGLINE allows you to maintain network-wide bans by
real name (gecos). It works similarly to AKILL.
<p>
<strong>Syntax:</strong> <tt>SGLINE ADD &lt;gecos&gt; [!P|!T &lt;minutes&gt;] &lt;reason&gt;</tt><br>
<p>
If the !P token is specified the SGLINE will never expire (permanent).
If the !T token is specified expire time must follow, in minutes,
hours ("h"), days ("d") or weeks ("w").
<p>
<strong>Examples:</strong>
<br><tt>/msg OperServ SGLINE ADD foo !T 5 bar reason</tt>
<br><tt>/msg OperServ SGLINE ADD foo !T 3d bar reason</tt>
<br><tt>/msg OperServ SGLINE ADD foo !P foo reason</tt>
<br><tt>/msg OperServ SGLINE ADD foo foo reason</tt>
<p>
The first example looks for the user with a gecos of "foo" and adds
a 5 minute SGLINE for "bar reason."
<p>
The second example is similar but adds the SGLINE for 3 days instead of
5 minutes.
<p>
The third example adds a permanent SGLINE on foo for "foo reason."
<p>
The fourth example adds a SGLINE on foo for the duration specified
in the configuration file for "foo reason."
<p>
<strong>Syntax:</strong> <tt>SGLINE DEL &lt;gecos|number&gt;</tt><br>
<p>
If number is specified it correlates with the number on SGLINE LIST.
You may specify multiple numbers by separating with commas.
You may specify a range by using a colon.
<p>
<strong>Examples:</strong>
<br><tt>/msg OperServ SGLINE DEL foo</tt>
<br><tt>/msg OperServ SGLINE DEL 5</tt>
<br><tt>/msg OperServ SGLINE DEL 1,2,5,10</tt>
<br><tt>/msg OperServ SGLINE DEL 1:5,7,9:11</tt>
<p>
<strong>Syntax:</strong> <tt>SGLINE LIST [FULL]</tt><br>
<p>
If FULL is specified the SGLINE reasons will be shown.
<p>
<strong>Examples:</strong>
<br><tt>/msg OperServ SGLINE LIST</tt>
<br><tt>/msg OperServ SGLINE LIST FULL</tt>
<p>
<strong>Syntax:</strong> <tt>SGLINE SYNC</tt><br>
<p>
Sends all sglines to all servers. This can be useful in case
services will be down or do not see a user as matching a
certain sgline.
<a name="SHUTDOWN"><h4>SHUTDOWN</h4></a>
<p>
SHUTDOWN shuts down services. Services will
not reconnect or restart.
<p>
<strong>Syntax:</strong> <tt>SHUTDOWN</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ SHUTDOWN</tt>
<a name="SOPER"><h4>SOPER</h4></a>
<p>
SOPER allows manipulation of services operator privileges.
<p>
SOPER LIST shows all accounts with services operator
privileges, both from the configuration file and
this command. It is similar to /stats o OperServ.
<p>
SOPER LISTCLASS shows all defined oper classes. Use
the SPECS command to view the privileges associated
with an oper class.
<p>
SOPER ADD grants services operator privileges to an
account. The granted privileges are described by an
oper class. You can also optionally specify a
password for the new services operator.
<p>
SOPER DEL removes services operator privileges from
an account.
<p>
SOPER SETPASS sets or clears a password for services
operator privileges on an account. The password must
be already encrypted. The target user needs to enter
the password using IDENTIFY.
<p>
It is not possible to modify accounts with
operator{} blocks in the configuration file.
<p>
<strong>Syntax:</strong> <tt>SOPER LIST|LISTCLASS</tt><br>
<strong>Syntax:</strong> <tt>SOPER ADD &lt;account&gt; &lt;operclass&gt; [password]</tt><br>
<strong>Syntax:</strong> <tt>SOPER DEL &lt;account&gt;</tt><br>
<strong>Syntax:</strong> <tt>SOPER SETPASS &lt;account&gt; [password]</tt><br>
<p>
<strong>Examples:</strong>
<br><tt>/msg OperServ SOPER LIST</tt>
<br><tt>/msg OperServ SOPER ADD anoper sra</tt>
<br><tt>/msg OperServ SOPER ADD newoper sra $1$bllsww$xBjenkPsZgkqy1Rx5gl2h1</tt>
<br><tt>/msg OperServ SOPER DEL abusiveoper</tt>
<br><tt>/msg OperServ SOPER SETPASS anoper $1$vHFzU0jC$ePfKvERVwaDRdnHOnZZ6h.</tt>
<a name="SPECS"><h4>SPECS</h4></a>
<p>
SPECS shows the privileges you have in services.
<p>
<strong>Syntax:</strong> <tt>SPECS</tt><br>
<p>
It is also possible to see the privileges of other
online users or of oper classes.
<p>
<strong>Syntax:</strong> <tt>SPECS USER &lt;nick&gt;</tt><br>
<strong>Syntax:</strong> <tt>SPECS OPERCLASS &lt;classname&gt;</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ SPECS USER w00t</tt>
<a name="SQLINE"><h4>SQLINE</h4></a>
<p>
SQLINE allows you to deny the use of certain nicknames
or channels network-wide.
<p>
A nickname sqline may contain *, ?, # (any digit) and
@ (any letter) wildcards. A channel sqline must be an
exact match, starting with # or &.
<p>
<strong>Syntax:</strong> <tt>SQLINE ADD &lt;mask&gt; [!P|!T &lt;minutes&gt;] &lt;reason&gt;</tt><br>
<p>
If the !P token is specified the SQLINE will never expire (permanent).
If the !T token is specified expire time must follow, in minutes,
hours ("h"), days ("d") or weeks ("w").
<p>
<strong>Examples:</strong>
<br><tt>/msg OperServ SQLINE ADD spambot* !T 7d bar reason</tt>
<br><tt>/msg OperServ SQLINE ADD spam??? !P foo reason</tt>
<p>
The first example denies the use of nicknames starting with
"spambot" for 7 days.
<p>
The second example adds a permanent SQLINE on "spam???" for "foo reason."
<p>
<strong>Syntax:</strong> <tt>SQLINE DEL &lt;mask|number&gt;</tt><br>
<p>
If number is specified it correlates with the number on SQLINE LIST.
You may specify multiple numbers by separating with commas.
You may specify a range by using a colon.
<p>
<strong>Examples:</strong>
<br><tt>/msg OperServ SQLINE DEL foo</tt>
<br><tt>/msg OperServ SQLINE DEL 5</tt>
<br><tt>/msg OperServ SQLINE DEL 1,2,5,10</tt>
<br><tt>/msg OperServ SQLINE DEL 1:5,7,9:11</tt>
<p>
<strong>Syntax:</strong> <tt>SQLINE LIST [FULL]</tt><br>
<p>
If FULL is specified the SQLINE reasons will be shown.
<p>
<strong>Examples:</strong>
<br><tt>/msg OperServ SQLINE LIST</tt>
<br><tt>/msg OperServ SQLINE LIST FULL</tt>
<p>
<strong>Syntax:</strong> <tt>SQLINE SYNC</tt><br>
<p>
Sends all sqlines to all servers. This is useful because
sqlines must be present before the nickname or channel
is tried to be fully effective.
<a name="UPDATE"><h4>UPDATE</h4></a>
<p>
UPDATE flushes the database to disk.
<p>
<strong>Syntax:</strong> <tt>UPDATE</tt><br>
<p>
<strong>Example:</strong>
<br><tt>/msg OperServ UPDATE</tt>
<a name="UPTIME"><h4>UPTIME</h4></a>
<p>
UPTIME shows services uptime and the number of
registered nicks and channels.
<p>
<strong>Syntax:</strong> <tt>UPTIME</tt><br>