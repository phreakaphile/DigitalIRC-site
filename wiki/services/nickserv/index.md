---
layout: wiki
title: Nickserv
---

Nickname management service that provides [nickname registration]({{site.baseurl}}/wiki/services/nickserv/register/)

<h3>ACC</h3>

ACC returns parsable information about a user's
login status. Note that on many networks, /whois
shows similar information faster and more reliably.
The answer is in the form &lt;nick&gt; ACC &lt;digit&gt;:
<br><tt>0 - account or user does not exist</tt>
<br><tt>1 - account exists but user is not logged in</tt>
<br><tt>2 - user is not logged in but recognized (see ACCESS)</tt>
<br><tt>3 - user is logged in</tt>

If the account is omitted the user's nick is used.
Account * means the account the user is logged in with.

<strong>Syntax:</strong> <tt>ACC</tt><br>
<strong>Syntax:</strong> <tt>ACC &lt;nick&gt;</tt><br>
<strong>Syntax:</strong> <tt>ACC &lt;nick&gt; &lt;account&gt;</tt><br>
<strong>Syntax:</strong> <tt>ACC &lt;nick&gt; *</tt><br>

<strong>Example:</strong>
<br><tt>/msg NickServ ACC jilles *</tt>
<h3>ACCESS</h3>

ACCESS maintains a list of user@host masks from where
NickServ will recognize you, so it will not prompt you to
change nick. Preventing expiry, getting channel access
or editing nickname settings still requires
identification, however.

Access list entries can use hostnames with optional
wildcards, IP addresses and CIDR masks. There are
restrictions on how much you can wildcard. If you omit
the mask, NickServ will attempt to generate one matching
your current connection.

<strong>Syntax:</strong> <tt>ACCESS LIST</tt><br>
<strong>Syntax:</strong> <tt>ACCESS ADD [mask]</tt><br>
<strong>Syntax:</strong> <tt>ACCESS DEL &lt;mask&gt;</tt><br>

Operators with user:auspex privilege can also
view another user's access list.

<strong>Syntax:</strong> <tt>ACCESS LIST &lt;nick&gt;</tt><br>

<strong>Examples:</strong>
<br><tt>/msg NickServ ACCESS LIST</tt>
<br><tt>/msg NickServ ACCESS ADD jack@host.example.com</tt>
<br><tt>/msg NickServ ACCESS ADD user@10.0.0.8</tt>
<br><tt>/msg NickServ ACCESS ADD jilles@192.168.1.0/24</tt>
<br><tt>/msg NickServ ACCESS DEL *someone@*.area.old.example.net</tt>

<h3>AJOIN</h3>

Maintains the AutoJoin list for nick group. If a user identifies to his nickname, he will automatically join the listed channels.

<strong>Syntax:</strong> <tt>AJOIN ADD</tt><br>
<strong>Syntax:</strong> <tt>AJOIN DEL</tt><br>
<strong>Syntax:</strong> <tt>AJOIN LIST</tt><br>
<strong>Syntax:</strong> <tt>AJOIN CLEAR</tt><br>

<strong>Examples:</strong>
<br><tt>/msg NickServ AJOIN LIST</tt>
<br><tt>/msg NickServ AJOIN ADD #help</tt>
<br><tt>/msg NickServ AJOIN DEL #TheZoo</tt>

<h3>DROP</h3>

Using this command makes NickServ remove your account
and stop watching your nick(s), If a nick is dropped,
anyone else can register it. You will also lose all
your channel access and memos.

When dropping and re-registering an account during a
netsplit, users on the other side of the split may later
be recognized as the new account.

<strong>Syntax:</strong> <tt>DROP &lt;nickname&gt; &lt;password&gt;</tt><br>

<strong>Examples:</strong>
<br><tt>/msg NickServ DROP foo bar</tt>
<h3>FDROP</h3>

FDROP forcefully removes the given account, including
all nicknames, channel access and memos attached to it.

When dropping and re-registering an account during a
netsplit, users on the other side of the split may later
be recognized as the new account.

<strong>Syntax:</strong> <tt>FDROP &lt;nickname&gt;</tt><br>

<strong>Examples:</strong>
<br><tt>/msg NickServ FDROP foo</tt>
<h3>FREEZE</h3>

FREEZE allows operators to "freeze" an abusive user's
account. This logs out all sessions logged in to the
account and prevents further logins. Thus, users
cannot obtain the access associated with the account.

FREEZE information will be displayed in INFO output.

<strong>Syntax:</strong> <tt>FREEZE &lt;nick&gt; ON|OFF &lt;reason&gt;</tt><br>

<strong>Examples:</strong>
<br><tt>/msg NickServ FREEZE pfish ON Persistent spammer</tt>
<br><tt>/msg NickServ FREEZE alambert OFF</tt>
<h3>FUNGROUP</h3>

FUNGROUP forcefully unregisters the given nickname
from the account it is registered to.

If you are ungrouping an account name, you need to
specify a new name for the account. This must be
another nick registered to it.
You cannot ungroup account names.

<strong>Syntax:</strong> <tt>FUNGROUP &lt;nickname&gt;</tt><br>
<strong>Syntax:</strong> <tt>FUNGROUP &lt;account&gt; &lt;newname&gt;</tt><br>

<strong>Examples:</strong>
<br><tt>/msg NickServ FUNGROUP SomeNick</tt>
<br><tt>/msg NickServ FUNGROUP SomeName SomeNick</tt>
<h3>GHOST</h3>

GHOST disconnects an old user session, or somebody
attempting to use your nickname without authorization.

If you are logged in to the nick's account, you need
not specify a password, otherwise you have to.

<strong>Syntax:</strong> <tt>GHOST &lt;nick&gt; [password]</tt><br>

<strong>Example:</strong>
<br><tt>/msg NickServ GHOST foo bar</tt>
<h3>GROUP</h3>

GROUP registers your current nickname to your account.
This means that NickServ protects this nickname the
same way as it protects your account name. Most
services commands will accept the new nickname as
an alias for your account name.

Please note that grouped nicks expire separately
from accounts. To prevent this, you must use them.
Otherwise, all properties of the account are shared
among all nicks registered to it.

<strong>Syntax:</strong> <tt>GROUP</tt><br>

<strong>Examples:</strong>
<br><tt>/msg NickServ IDENTIFY OldNick SecretPassword</tt>
<br><tt>/msg NickServ GROUP</tt>
<h3>HOLD</h3>

HOLD prevents an account and all nicknames registered
to it from expiring.

<strong>Syntax:</strong> <tt>HOLD &lt;nick&gt; ON|OFF</tt><br>

<strong>Examples:</strong>
<br><tt>/msg NickServ HOLD jilles ON</tt>
<h3>IDENTIFY</h3>

IDENTIFY identifies you with services so that you
can perform general maintenance and commands that
require you to be logged in.

<strong>Syntax:</strong> <tt>IDENTIFY &lt;password&gt;</tt><br>

You can also identify for another nick than you
are currently using.

<strong>Syntax:</strong> <tt>IDENTIFY &lt;nick&gt; &lt;password&gt;</tt><br>

<strong>Example:</strong>
<br><tt>/msg NickServ IDENTIFY foo</tt>
<br><tt>/msg NickServ IDENTIFY jilles foo</tt>
<h3>INFO</h3>

INFO displays account information such as
registration time, flags, and other details.
Additionally it will display registration
and last seen time of the nick you give.

You can query the nick a user is logged in as
by specifying an equals sign followed by their
nick. This '=' convention works with most commands.

<strong>Syntax:</strong> <tt>INFO &lt;nickname&gt;</tt><br>
<strong>Syntax:</strong> <tt>INFO =&lt;online user&gt;</tt><br>

<strong>Examples:</strong>
<br><tt>/msg NickServ INFO w00t</tt>
Shows information about the registered nick w00t.
<br><tt>/msg NickServ INFO =w00tie[home]</tt>
Shows information about the registered nick the user
w00tie[home] is logged in as.
<h3>LIST</h3>

LIST shows registered users that match a given criteria.
Multiple criteria may be used in the same command.

Current Criteria are:
PATTERN       - All users that match a given pattern.
EMAIL         - All accounts registered with an email address
<br><tt>            that matches a given pattern.</tt>
MARK-REASON   - All accounts whose mark reason matches a
<br><tt>            given pattern.</tt>
FROZEN-REASON - All frozen accounts whose freeze reason matches
<br><tt>            a given pattern.</tt>
HOLD          - All users with the HOLD flag set.
NOOP          - All users with the NOOP flag set.
NEVEROP       - All users with the NEVEROP flag set.
WAITAUTH      - All users with the WAITAUTH flag set.
HIDEMAIL      - All users with the HIDEMAIL flag set.
NOMEMO        - All users with the NOMEMO flag set.
EMAILMEMOS    - All users with the EMAILMEMOS flag set.
USE-PRIVMSG   - All users with the USEPRIVMSG flag set.
QUIETCHG      - All users with the QUIETCHG flag set.
NOGREET       - All users with the NOGREET flag set.
PRIVATE       - All users with the PRIVATE flag set.
REGNOLIMIT    - All users with the REGNOLIMIT flag set.

FROZEN        - All users frozen by network staff.
MARKED        - All users marked by network staff.
REGISTERED    - User accounts registered longer ago than a given age.
LASTUSED      - User accounts last used longer ago than a given age.

<strong>Syntax:</strong> <tt>LIST &lt;criteria&gt;</tt><br>

<strong>Examples:</strong>
<br><tt>/msg NickServ LIST pattern foo*</tt>
<br><tt>/msg NickServ LIST hold</tt>
<br><tt>/msg NickServ LIST frozen pattern x*</tt>
<br><tt>/msg NickServ LIST registered 30d</tt>
<br><tt>/msg NickServ LIST marked registered 7d pattern bar</tt>
<br><tt>/msg NickServ LIST email *@gmail.com</tt>
<br><tt>/msg NickServ LIST mark-reason *lamer*</tt>
<h3>LISTCHANS</h3>

LISTCHANS shows the channels that you have access
to, including those that you own.

AKICKs and host-based access are not shown.

<strong>Syntax:</strong> <tt>LISTCHANS</tt><br>

Operators with chan:auspex privilege can also
check another user's access.

<strong>Syntax:</strong> <tt>LISTCHANS &lt;nick&gt;</tt><br>

<strong>Example:</strong>
<br><tt>/msg NickServ LISTCHANS</tt>
<h3>LISTVHOST</h3>

LISTVHOST shows accounts which have a vhost set
on them. If a pattern is given, only accounts
with vhosts matching the pattern are shown.

<strong>Syntax:</strong> <tt>LISTVHOST [pattern]</tt><br>

<strong>Examples:</strong>
<br><tt>/msg NickServ LISTVHOST</tt>
<br><tt>/msg NickServ LISTVHOST *staff*</tt>
<h3>LOGIN</h3>

LOGIN identifies you with services so that you
can perform general maintenance and commands that
require you to be logged in.

<strong>Syntax:</strong> <tt>LOGIN &lt;account&gt; &lt;password&gt;</tt><br>

<strong>Example:</strong>
<br><tt>/msg NickServ LOGIN smith sesame</tt>
<h3>LOGOUT</h3>

LOGOUT logs you out of the account
that you are currently logged into.

<strong>Syntax:</strong> <tt>LOGOUT</tt><br>

<strong>Example:</strong>
<br><tt>/msg NickServ LOGOUT</tt>
<h3>MARK</h3>

MARK allows operators to attach a note to an account.
For example, an operator could mark the account of
a spammer so that others know the user has previously
been warned.

MARK information will be displayed in INFO output.

<strong>Syntax:</strong> <tt>MARK &lt;nickname&gt; ON|OFF &lt;reason&gt;</tt><br>

<strong>Examples:</strong>
<br><tt>/msg NickServ MARK game_boy ON Persistent spammer</tt>
<br><tt>/msg NickServ MARK nenolod OFF</tt>
<h3>REGAIN</h3>

REGAIN regains access to your nickname from
a user that is using your nick.

If you are logged in to the account associated with
the nickname, you need not specify a password,
otherwise you have to.

<strong>Syntax:</strong> <tt>REGAIN &lt;nick&gt; [password]</tt><br>

<strong>Example:</strong>
<br><tt>/msg NickServ REGAIN Dave2 goats</tt>
<h3>REGISTER</h3>

This will register your current nickname with NickServ.
This will allow you to assert some form of identity on
the network and to be added to access lists. Furthermore,
NickServ will warn users using your nick without
identifying and allow you to kill ghosts.
The password is a case-sensitive password that you make
up. Please write down or memorize your password! You
will need it later to change settings.

You have to confirm the email address. To do this,
follow the instructions in the message sent to the email
address.

<strong>Syntax:</strong> <tt>REGISTER &lt;password&gt; &lt;email-address&gt;</tt><br>

<strong>Examples:</strong>
<br><tt>/msg NickServ REGISTER bar foo@bar.com</tt>
<h3>REGNOLIMIT</h3>

REGNOLIMIT allows a user to maintain an
unlimited amount of channel registrations.

<strong>Syntax:</strong> <tt>REGNOLIMIT &lt;user&gt; ON|OFF</tt><br>

<strong>Examples:</strong>
<br><tt>/msg NickServ REGNOLIMIT nenolod ON</tt>
<h3>RELEASE</h3>

RELEASE removes an enforcer for your nick or
changes the nick of a user that is using your
nick.

Enforcers are created when someone uses your
nick without identifying and prevent all use
of it.

If you are logged in to the nick, you need not specify
a password, otherwise you have to.

<strong>Syntax:</strong> <tt>RELEASE &lt;nick&gt; [password]</tt><br>

<strong>Example:</strong>
<br><tt>/msg NickServ RELEASE smith sesame</tt>
<h3>RESETPASS</h3>

RESETPASS sets a random password for the specified
account.

<strong>Syntax:</strong> <tt>RESETPASS &lt;nickname&gt;</tt><br>

<strong>Examples:</strong>
<br><tt>/msg NickServ RESETPASS pfish</tt>
<h3>RESTRICT</h3>

RESTRICT allows operators to restrict what an account can and can not do.
It will currently block a user from grouping nicks, registering a channel,
requesting a vhost or taking an OFFERed vhost.

This is particularly useful if a user starts abusing these commands.

<strong>Syntax:</strong> <tt>RESTRICT &lt;nickname&gt; ON|OFF &lt;reason&gt;</tt><br>

<strong>Examples:</strong>
<br><tt>/msg NickServ RESTRICT game_boy ON Abusing vhost requests</tt>
<br><tt>/msg NickServ RESTRICT nenolod OFF</tt>
<h3>RETURN</h3>

RETURN resets the specified account
password, sends it to the email address
specified and changes account's email address
to this address. Any current sessions logged
in to the account are logged out.

<strong>Syntax:</strong> <tt>RETURN &lt;nickname&gt; &lt;e-mail&gt;</tt><br>

<strong>Examples:</strong>
<br><tt>/msg NickServ RETURN jdoe john@example.com</tt>
<h3>SENDPASS</h3>
<h4><strong>Digital IRC does not use this. Please file a password reset with the <a href="http://tickets.digitalirc.org/">ticket system</a></strong></h4>

SENDPASS emails the password for the specified
nickname to the corresponding email address.
SENDPASS emails a key to the email address
corresponding to the specified nickname
that can be used to set a new password
using SETPASS.

<strong>Syntax:</strong> <tt>SENDPASS &lt;nickname&gt;</tt><br>

If the nickname is marked, you can override this
using the FORCE keyword.

<strong>Syntax:</strong> <tt>SENDPASS &lt;nickname&gt; FORCE</tt><br>

If a key has been emailed but not yet used,
you can clear it using the CLEAR keyword.

<strong>Syntax:</strong> <tt>SENDPASS &lt;nickname&gt; CLEAR</tt><br>

<strong>Examples:</strong>
<br><tt>/msg NickServ SENDPASS foo</tt>
<h3>SENDPASS</h3>

SENDPASS emails a key to the email address
corresponding to the specified nickname
that can be used to set a new password
using SETPASS.

<strong>Syntax:</strong> <tt>SENDPASS &lt;nickname&gt;</tt><br>

If a key has been emailed but not yet used,
you can clear it using the CLEAR keyword.

<strong>Syntax:</strong> <tt>SENDPASS &lt;nickname&gt; CLEAR</tt><br>

<strong>Examples:</strong>
<br><tt>/msg NickServ SENDPASS foo</tt>
<h3>SET EMAIL</h3>

SET EMAIL changes the e-mail address
associated with an account. The e-mail
address is used for password retrieval.

You may be required to confirm the new
e-mail address. To confirm the address,
follow the instructions in the message
sent to the new address.

<strong>Syntax:</strong> <tt>SET EMAIL &lt;new address&gt;</tt><br>

<strong>Example:</strong>
<br><tt>/msg NickServ SET EMAIL dan@example.com</tt>
<h3>SET ENFORCE</h3>

SET ENFORCE allows you to enable more protection for
all nicknames registered to your account.

This will automatically change the nick of someone
who attempts to use it without identifying in time,
and temporarily block its use, which can be
removed at your discretion. See help on RELEASE.

<strong>Syntax:</strong> <tt>SET ENFORCE ON|OFF</tt><br>
<h3>SET HIDEMAIL</h3>

SET HIDEMAIL prevents an account's e-mail address
from being shown to other users.

<strong>Syntax:</strong> <tt>SET HIDEMAIL ON|OFF</tt><br>

<strong>Example:</strong>
<br><tt>/msg NickServ SET HIDEMAIL ON</tt>
<h3>SET LANGUAGE</h3>

SET LANGUAGE changes the language services
uses to communicate with you.

<strong>Syntax:</strong> <tt>SET LANGUAGE &lt;abbreviation&gt;</tt><br>

<strong>Example:</strong>
<br><tt>/msg NickServ SET LANGUAGE en</tt>
<br><tt>/msg NickServ SET LANGUAGE ru</tt>
<h3>SET NEVEROP</h3>

SET NEVEROP prevents others from adding you to
channel access lists.

<strong>Syntax:</strong> <tt>SET NEVEROP ON|OFF</tt><br>

<strong>Example:</strong>
<br><tt>/msg NickServ SET NEVEROP ON</tt>
<h3>SET NOMEMO</h3>

This prevents people from being able to send you
a memo. If you do not want to receive memos, you
can just turn them off for your nick.

<strong>Syntax:</strong> <tt>SET NOMEMO ON|OFF</tt><br>

<strong>Example:</strong>
<br><tt>/msg NickServ SET NOMEMO ON</tt>
<h3>SET NOOP</h3>

SET NOOP prevents services from automatically
opping you in channels you have access in.
You can choose to op/voice yourself by using
the OP and VOICE commands.

<strong>Syntax:</strong> <tt>SET NOOP ON|OFF</tt><br>

<strong>Example:</strong>
<br><tt>/msg NickServ SET NOOP ON</tt>
<h3>SET PASSWORD</h3>

SET PASSWORD changes the password of an account.

<strong>Syntax:</strong> <tt>SET PASSWORD &lt;new password&gt;</tt><br>

<strong>Example:</strong>
<br><tt>/msg NickServ SET PASSWORD swordfish</tt>
<h3>SET PRIVATE</h3>

SET PRIVATE hides various information about your
account from other users.

SET PRIVATE ON automatically enables HIDEMAIL too.

<strong>Syntax:</strong> <tt>SET PRIVATE ON|OFF</tt><br>

<strong>Example:</strong>
<br><tt>/msg NickServ SET PRIVATE ON</tt>
<h3>SET QUIETCHG</h3>

SET QUIETCHG prevents services from automatically
notifying you when ChanServ is used to affect your
status in channels. When set to ON, Services will no
longer send you messages of this nature.

<strong>Syntax:</strong> <tt>SET QUIETCHG ON|OFF</tt><br>

<strong>Example:</strong>
<br><tt>/msg NickServ SET QUIETCHG ON</tt>
<h3>SETPASS</h3>

SETPASS allows you to set a new password
using a key emailed to you. The key is
valid for one time only, and also becomes
invalid if you identify with your old password.

To set a new password if you know the current
password, use SET PASSWORD instead of SETPASS.

<strong>Syntax:</strong> <tt>SETPASS &lt;nickname&gt; &lt;key&gt; &lt;password&gt;</tt><br>
<h3>STATUS</h3>

STATUS returns information about your current
state. It will show information about your
nickname, IRC operator, and SRA status.

<strong>Syntax:</strong> <tt>STATUS</tt><br>

<strong>Example:</strong>
<br><tt>/msg NickServ STATUS</tt>
<h3>UNGROUP</h3>

UNGROUP unregisters the given nickname from your
account. The nickname will be available for others to
register. This will not affect your channel access
or memos.

If you do not specify a nickname, your current
nickname will be ungrouped. You cannot ungroup
your account name.

<strong>Syntax:</strong> <tt>UNGROUP [nickname]</tt><br>

<strong>Examples:</strong>
<br><tt>/msg NickServ UNGROUP SomeNick</tt>
<h3>VHOST</h3>

VHOST allows operators to set a virtual host (also
known as a spoof or cloak) on an account. This vhost
will be set on the user immediately and each time
they identify.

<strong>Syntax:</strong> <tt>VHOST &lt;nickname&gt; ON &lt;vhost&gt;</tt><br>
<strong>Syntax:</strong> <tt>VHOST &lt;nickname&gt; OFF</tt><br>

If the nickname is marked, you can override this
using the FORCE keyword.

<strong>Syntax:</strong> <tt>VHOST &lt;nickname&gt; ON &lt;vhost&gt; FORCE</tt><br>
<strong>Syntax:</strong> <tt>VHOST &lt;nickname&gt; OFF FORCE</tt><br>

<strong>Examples:</strong>
<br><tt>/msg NickServ VHOST spb ON may.explode.on.impact</tt>
<br><tt>/msg NickServ VHOST nenolod OFF</tt>
