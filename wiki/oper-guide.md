---
layout: wiki
title: Oper Guide
---

## What is an IRCop?

The term IRCop stands for IRC Operator -- not IRC Cop or mIRC Cop as some believe (it's important to make the distinction between IRC and mIRC since mIRC is a program used to access IRC; there are many other IRC clients besides mIRC). They may also be referred to as an oper.

IRCops are users who have access to commands that allow them to administrate their server or the network. They usually have more experience with IRC and the network than normal users, and are therefore qualified to deal with problematic situations. Please note that they are all volunteers and do not get paid for their work.

## What are the responsibilities of an IRCop?

The primary function of an IRCop is to see that their server, and the network as a whole, are running smoothly. That includes removing clones, flooders or mass advertisers (spammers) and disconnecting and reconnecting servers to fix significant lag or netsplits (a good explanation of netsplits is available at [http://docs.dal.net/docs/netsplits.html](http://docs.dal.net/docs/netsplits.html)).

An IRCop may also choose to spend some of their time helping users. For instance, one of the most common reasons for seeking out an IRCop is because you have forgotten your password.

## What does an IRCop not do?

Contrary to common belief, IRCops cannot just do whatever they feel like. They were chosen as opers for the good of their server and the network and, as such, are expected to follow the rules and behave in a responsible manner befitting their status.

IRCops will not start dropping or stealing nicknames and channels for the fun of it. Everything they do is logged and any abusive actions are dealt with severely (just as chachin in #TheZoo). However, this does not mean that IRCops will not take action against abusive users, be they spammers, cloners, Services abusers or other troublemakers. There are also myths involving powers that do not exist. For example, it is not possible for IRCops to listen in on someone's private conversation.

This means that IRCops will not drop nicknames or channels that are on the verge of expiring. During Services outages, they are very unlikely to op you in your channels since there's no way for them to verify your identity (in other words, your right to be opped in the channel) with absolute certainty. However, they will help you with cloning and flooding problems.

Keep in mind that the rules state that channel founders may run channels however they wish as long as they meet the network rules, even if they are unfair, as long as they don't abuse Services. There are no rules that state that channel founders should be fair -- if you don't like the way some channel is run, there are many others to try. Thus IRCops don't have any authority in internal channel matters of this type.

## What are the different "types" of IRCops?

IRCops do not have identical powers. There are different categories of them and each one has access to different commands. Generally speaking, a category has access to the commands of the previously listed ones (with the exception of server admins). As such, the higher categories also face a lot more responsibilities and duties. The following should give you a general idea of what they can do.

#### Local Operator

    A local IRCop is only shown to be an oper if you are checking the WHOIS information from the server that they are using. They have the ability to set k:lines (local server bans), to connect their server, to disconnect users on their server, to see users who are set to +i and other limited abilities.

    Generally speaking, local opers can only affect things on their particular server and not on the rest of the network.

#### Global Operator

    A global IRCop is shown to be an oper, no matter what server you happen to be on. They can disconnect users from the network, have the ability to see ChanServ access lists (aop/sop/count), etc.

Unlike local opers, global opers can affect the entire network through their actions.

#### Server Administrator

    A Server Administrator is the administrator of a server. They are responsible for the primary opers on their server and the well-being of the server in general. There can be up to two server administrators for a given server.

#### Network Administrator

    A Network Administrator is in charge of maintaining the health of the entire network. They have the highest access level.

## How do I complain about an IRCop?

The easiest way to complain about an IRC Operator is to submit a ticket about them via our ticket support system [tickets.digitalirc.org](http://tickets.digitalirc.org/open.php). You will be contacted via this system when your abuse report is being dealt with.

---

This guide is based off of the exelent [oper guide by DALnet](http://docs.dal.net/docs/operinfo.html)
