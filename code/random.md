---
layout: page
title: DigitalIRC Random bits of code
---

### About

**author** [MrRandom](http://twitter.com/digitalirc/)

**author email** mrrandom [at] digitalirc [dot] org

**copyright** DigitalIRC Network 2012 - 2013

**version** 1.1

Random stuff thats nessisary for the network to "work"

### IRC


Contains scripts and files for various functions of the IRC.
Notable Scripts:

* [unreal-update.sh][1]: update server's from unreal 3.2.9 to 3.2.10

* [update.sh][2]: used to update motd files using contents of DigitalIRC-motd

### deploy-files


Contains files for deploying new servers.

### deploy-scripts


Contains scripts for deploying new servers.
Notable Scripts:

* [ircd-deploy][3]: creates ircd users, calls ircd installer, and installs the motd generator.

[1]: https://github.com/DigitalIRC/random/blob/master/IRC/unreal-update.sh
[2]: https://github.com/DigitalIRC/random/blob/master/IRC/update.sh
[3]: https://github.com/DigitalIRC/random/blob/master/deploy-scripts/ircd-deploy.sh