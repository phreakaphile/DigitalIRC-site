---
title: New Network stats website
author: MrRandom
layout: post
---
We have launched a new shiny stats website which uses html5 and css3 it&#8217;s reachable at [stats.digitalirc.org][1].

The old stats site will be available for the time being as well at it&#8217;s old address. [www.digitalirc.org/stats][2]

<span style="line-height: 1.714285714; font-size: 1rem;">- MrR</span>

 [1]: http://stats.digitalirc.org
 [2]: http://www.digitalirc.org/stats/
