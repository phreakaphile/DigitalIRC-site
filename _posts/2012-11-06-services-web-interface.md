---
title: Services web interface
author: MrRandom
layout: post
---
We have added a web interface for our services package.

This will allow you to change and request new vhosts via hostserv, manage channels you have access on, change and view basic nickserv information and send memos to other users which can be picked up either via the web interface or on IRC.

New users can also register via IRC as well.

This new service is available at <http://www.digitalirc.org/services/> or at the right of your screen under IRC Links
