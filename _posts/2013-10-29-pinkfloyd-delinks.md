---
title: Pinkfloyd delinks from the network & other updates
author: MrRandom
layout: post
---

Sad news, we have received confirmation from ^tammy^ that pinkfloyd will be permanently delinking from the network.

This would leave our western US coverage pretty thin but the provider of tubes has agreed to move it to a datacenter in LA!

We have also changed the way our main round robbin ([irc.digitalirc.org][irc]). It will now forward you to one of the geographic round robins based on your location.


[irc]: irc://irc.digitalirc.org:6667/
