---
title: Welcome tubes to the network
author: MrRandom
layout: post
---
Hey,

Tubes has been added to the IRC Network. While it is open to outside connections it is primarily aimed to be the new host for the bouncers, since it has more power and a better processor.

It does not current support IPv6 native connections and has SSL on the standard port (6697)

&nbsp;

-MrR
