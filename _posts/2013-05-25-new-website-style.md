---
layout: post
title: New website style
category: Informational
excerpt: We have a nice new website with shiny new graphics.
---

We've moved the site from wordpress to jekyll.

Jekyll is a simple, blog-aware, static site generator. It takes a template directory containing raw text files in various formats, runs it through Markdown and Liquid converters, and spits out a complete, ready-to-publish static website suitable for serving.

Since it now generatates a static site, it makes it very easy to deploy with little configuration needed on the server side; no php, no database, and no plugins.
