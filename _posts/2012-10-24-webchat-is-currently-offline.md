---
title: Webchat is currently offline
author: MrRandom
layout: post
---
Hey,  
We are currently having problems with the webchat on its current host. An alternat webchat is available at <a title="Alternate Webchat" href="http://shire.digitalirc.org:9090/" target="_blank">shire.digitalirc.org:9090</a>. We are investigating the problem and will make another post if the situation changes.

&nbsp;

MrR

&nbsp;

Update &#8211; The <a href="http://webchat.digitalirc.org/" target="_blank">webchat</a> is currently back up but it WILL NOT connect via ssl. We are still seeing what the problem is with ssl
