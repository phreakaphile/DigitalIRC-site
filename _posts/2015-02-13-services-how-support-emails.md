---
title: Reset your nickserv password via email
author: MrRandom
layout: post
except: Resetting a lost password just got easier
---

In a drive to make it easier to use our services we have now enabled resetting your password via email.

To do this all you need to do enter the following command into IRC

```
/msg nickserv sendpass <nickname>
```

This will cause an email to be sent to the associated email address with instructions on how to reset your password.

This feature is currently being trialled and may be removed at any point. All emails will be from services (a) digitalirc.org. As always if you have any problems feel free to pop into [#help]({{site.webchat}}help).

- MrRandom
