---
title: Anope to atheme
author: MrRandom
layout: post
---
As of 20 October 2012 services on DigitalIRC have moved from anope to Atheme. This move will provide more flexability for channel owner while maintaining the current functionality provided by anope, documentation on the site has been updated and all user data has been moved over.

As always if you have any problems feel free to drop by #help @ irc.digitalirc.org

A [How to guide has been created on the wiki][1].

 [1]: http://www.digitalirc.org/wiki/index.php?title=How_to_migrate_from_anope_to_atheme
