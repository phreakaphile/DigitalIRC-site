---
title: Network bouncers have moved
author: MrRandom
layout: post
---
Hey,

The network bouncers have now moved to a more powerful host and now have a dedicated hostname just in case we need to upgrade to a more powerful server again.

The bouncers are now available at <a title="DigitalIRC Bouncer Web Interface" href="https://bouncer.digitalirc.org:8000/" target="_blank">bouncer.digitalirc.org port 8000</a> for both IRC connections and for the web admin interface.

-MrR
