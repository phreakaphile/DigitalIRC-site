---
title: What is a vHost and how to get one.
author: MrRandom
layout: post
---
<span style="text-decoration:underline"><strong>What is a vHost</strong></span> 
A host is the part that comes after the @ when you join a channel on IRC (For example, \[you\] (webchat@SilentZombies-2C774A64.your.isp.net) ) has joined #empornium). A vHost (aka virtual host) is a feature on Empornium irc network, that allows you to be identified with a custom host, hiding your real ISP, making some silly joke, or even ban evasion (requesting one for ban evasion will get you banned from the entire network, not just a channel).

<span style="text-decoration:underline"><strong>Don&#8217;t I already have a vhost?</strong></span> 
By default you host is hidden by a standard vhost which everyone who registers gets (e.g. [you].users.SilentZombies ), this tutorial is if you want a custom one. For example MrRandom@Ego.sum.penitus.corrupta

<span style="text-decoration:underline"><strong>Rules for vhosts</strong></span> 
1. No IP vhosts, or IP look-a-likes. e.g. SilentZombies-7056744.isp.net or 96226D5B.61EF05EF.4702A4E2.IP  
2. If your vhost can, is, or could possibly be an actual domain you need to be able to prove you own that domain. e.g. google.com or empornium.me  
3. No impersonating private organizations which have government affiliation (MPAA, RIAA, etc.). We don&#8217;t want any trouble with them.  
4. No vhosts that contain the words forum, ircop, admin, network, or any IRCop&#8217;s nick. e.g. admin.irc or mrrandom.sucks  
5. You are only allowed to change your vhost once every 28 days. Spamming the system with frequent requests will result in a ban from the vhost system.  
6. If you are caught abusing vhosts to evade bans, you will have your vhost removed and banned from the network for a minimum of 3 days.  
7. No racist words. *Note &#8211; What is considered racist/derogatory is subject to discretion by vhost setter.  
8. No references to other networks/spamming. (e.g. come.to.irc.blahblahblah.net, join.my.channel)  
9. No references to &#8220;kiddy&#8221;-like activity.  
10. Your vhost must contain at least one period (**<span style="font-size:3px">.</span>**)  
11. We do not offer vIdents (the part before tha @ ) but if you use a seperate IRC Client you should be able to change your ident in it&#8217;s settings

**<span style="text-decoration:underline">How to request a vhost</span>** 
&nbsp;

So your vhost meets all the rules first you need to be registered with NickServ ([see this tutorial on how to register][1]). Once that is done simply enter:  
**Quote:**  
<table border="1">
  <tr>
    <td>
      <blockquote>
        /hs request your.vhost.here
      </blockquote>
    </td>
  </tr>
</table>

When it has been approved it will automatically set its self as your host, and each time you subsequently identify each time you connect.

 [1]: http://empornium.mooo.com/articles.php?topic=irc_register_nick
