---
title: Shiny new webchat
author: MrRandom
layout: post
except: We got a new shiny webchat
---

As you may have noticed we have now replaced our webchat with an entirely new, better, faster, and more reliable system.

Our old webchat was based off the atheme IRIS web client, which its self was based off of qwebIRC. This worked very well for us but we were having some stability issues with it. So onto our new system!

Our new webchat is based off of KiwiIRC running on nodeJS and uses websockets for updating the client. The new webchat should have support for all the features the old webchat had and more, though it will break the current embedded iframe's.

To generate a new iframe please use the generator we have provided for use with our webchat which can be [found here]({{site.baseurl}}/webchat/)

If you have any problems or queries please feel free to join pop over to [#help]({{site.webchat}}help)

- MrRandom