---
title: Welcome HollyWood to the network
author: MrRandom
layout: post
---

Please welcome [hollywood.digitalirc.org](irc://hollywood.digitalirc.org) to the network. It&#8217;s based in Orlando, Florida and it supports standard SSL (port 6697) and IPv6.

-MrR
